# Android Frameworks

[![Download](https://api.bintray.com/packages/wylhyz/maven/android-frameworks/images/download.svg)](https://bintray.com/wylhyz/maven/android-frameworks/_latestVersion)
[![Build Status](https://travis-ci.org/wylhyz/AndroidFrameworks.svg?branch=master)](https://travis-ci.org/wylhyz/AndroidFrameworks)


主要是android sdk框架的某些api的修改实现（一般是直接复制源代码然后修改一些参数的实现）

- 1.继承自AppCompatActivity的AccountAuthenticatorActivity
- 2.DiskLruCache 源码（摘自Google Android Sample [DiskLruCache](https://developer.android.com/samples/DisplayingBitmaps/src/com.example.android.displayingbitmaps/util/DiskLruCache.html)）

待续...



使用方法

project -> build.gradle
```gradle
allprojects {
    repositories {
        jcenter()

        maven {
            url 'https://dl.bintray.com/wylhyz/maven'
        }
    }
}
```

然后添加下面到项目module的build.gradle下
```gradle
compile 'io.lhyz.android:frameworks:0.0.1+'
```