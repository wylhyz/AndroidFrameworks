package io.lhyz.android.samples;

import android.os.Bundle;

import io.lhyz.android.frameworks.accounts.AccountAuthenticatorActivity;

public class AuthActivity extends AccountAuthenticatorActivity {

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_auth);
    }
}
